.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

SRC1_DIMM   EQU   8
SRC1_REAL   EQU   2
SRC2_DIMM   EQU   2048
GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
GetCurrentDirectoryA PROTO :DWORD, :DWORD
CreateDirectoryA PROTO :DWORD, :DWORD
StdOut PROTO :DWORD
StdIn PROTO :DWORD, :DWORD
FpuAtoFL PROTO :DWORD, :DWORD, :DWORD
FpuFLtoA PROTO :DWORD, :DWORD, :DWORD, :DWORD
.DATA


	lokalizacja BYTE "Podja liczbe",10,0

	zmienna1 BYTE 128 DUP(?)
	zmienna2 BYTE 128 DUP(0)

	zmienna1R REAL10 ?
	zmienna2R REAL10 ?
	wynik REAL10 0.0
	testo BYTE "5",0
.CODE
main proc


	invoke StdOut, OFFSET lokalizacja
	invoke StdIn, OFFSET zmienna1, 10
	invoke FpuAtoFl, OFFSET zmienna1, OFFSET zmienna1R, 0
	invoke StdOut, OFFSET lokalizacja
	invoke StdIn, OFFSET zmienna2, 10
	invoke FpuAtoFL, ADDR zmienna2, ADDR zmienna2R, 0
	 fld zmienna1R
	 fld zmienna2R
	 faddp st(1),st(0)
	  fstp wynik
	 invoke FpuFLtoA, ADDR wynik, 6, ADDR zmienna1, SRC1_REAL or SRC2_DIMM
	;	MOV EAX,zmienna1R
	 invoke StdOut,OFFSET zmienna1
	invoke ExitProcess, 0

main endp

END